const React = require('react');

class YourComponent extends React.Component {
    render () {
        return <div>Now you can create your own component package!</div>;
    }

}
module.exports = YourComponent;
