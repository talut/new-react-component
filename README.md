#### [Basic] New React Component generator for Yeoman

**You can create your own component package with this generator.**

**This generator create basic files for React Component package writing.**

The following files are for the creating React component
-  **src/index.js**
-  **.babelrc**
-  **.gitignore**
-  **.npmignore**
-  **package.json**
-  **README.md**


#### How To Use?

You must have Yeoman: [http://yeoman.io](http://yeoman.io)

First, you must run this command.

    npm install -g generator-new-react-component

After than you can use this generator with **yo**

    yo new-react-component

For publishing your package you must read **npm** documents : [https://docs.npmjs.com/getting-started/publishing-npm-packages](https://docs.npmjs.com/getting-started/publishing-npm-packages)
<p align="center">
<img src="https://gitlab.com/taluttasgiran/new-react-component/raw/master/res.png" />
</p>

*Please don't forget to give stars.*